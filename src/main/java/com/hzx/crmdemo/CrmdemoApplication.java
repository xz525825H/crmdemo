package com.hzx.crmdemo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan(value ="com.hzx.crmdemo.mapper" )
public class CrmdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrmdemoApplication.class, args);
    }

}
