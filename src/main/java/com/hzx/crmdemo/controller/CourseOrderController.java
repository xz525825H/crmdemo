package com.hzx.crmdemo.controller;

import com.hzx.crmdemo.entity.CURDResult;
import com.hzx.crmdemo.entity.CourseOrder;
import com.hzx.crmdemo.entity.PageResult;
import com.hzx.crmdemo.service.CourseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/courseorder")
public class CourseOrderController {

    @Autowired
    CourseOrderService courseOrderService;

    @RequestMapping("/list")
    public String getCourseList(){
        return "courseorder/list";
    }

    @RequestMapping("/detail")
    public String detail(Model model, String order_id){
        CourseOrder order= courseOrderService.findByOrderId(order_id);
        model.addAttribute("order",order);
        return "courseorder/detail";
    }

    @RequestMapping("/delete")
    @ResponseBody
    public CURDResult delete(String order_id){
        CURDResult result = new CURDResult();
        courseOrderService.deleteByOrderId(order_id);
        return result;
    }

    @RequestMapping("/add")
    public String add(){
        return "courseorder/add";
    }

    @RequestMapping("/edit")
    public String edit(Model model,String order_id){
        CourseOrder order= courseOrderService.findByOrderId(order_id);
        model.addAttribute("order",order);
        return "courseorder/edit";
    }

    @RequestMapping("/listJson")
    @ResponseBody     //添加该注解返回JSON格式数据
    public PageResult<CourseOrder> listjson(CourseOrder condition,Integer page,Integer limit){
        PageResult<CourseOrder> result = courseOrderService.findPageResult(condition,page,limit);
        return result;
    }

    @RequestMapping("/save")
    @ResponseBody
    public CURDResult save(CourseOrder order){
        CURDResult result =new CURDResult();
        if(order.getOrder_id() == null || order.getOrder_id().length() == 0){
            courseOrderService.save(order);
        }else {
            courseOrderService.update(order);
        }
        return result;
    }

}
