package com.hzx.crmdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/dataAnalysis")
public class DataAnalysisController {

    @RequestMapping("/income")
    public String income(){
        return "dataanalysis/income";
    }
}
