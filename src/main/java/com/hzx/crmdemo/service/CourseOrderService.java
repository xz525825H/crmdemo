package com.hzx.crmdemo.service;

import com.hzx.crmdemo.entity.CourseOrder;
import com.hzx.crmdemo.entity.PageResult;

public interface CourseOrderService {

    public PageResult<CourseOrder> findPageResult(CourseOrder condition,Integer page,Integer pagesize);

    public void save(CourseOrder order);

    public CourseOrder findByOrderId(String order_id);

    public void deleteByOrderId(String order_id);

    public void update(CourseOrder order);
}
