package com.hzx.crmdemo.service.Impl;

import com.hzx.crmdemo.entity.CourseOrder;
import com.hzx.crmdemo.entity.PageResult;
import com.hzx.crmdemo.mapper.CourseOrderMapper;
import com.hzx.crmdemo.service.CourseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CourseOrderServiceImpl implements CourseOrderService {

    @Autowired
    CourseOrderMapper courseOrderMapper;

    @Override
    public PageResult<CourseOrder> findPageResult(CourseOrder condition, Integer page, Integer pagesize) {

        PageResult<CourseOrder> result = new PageResult<CourseOrder>();
        result.setCode(0);

        Map<String,Object> params = new HashMap<String,Object>();
        params.put("condition",condition);
        //select * from t_course_order limit 0,10
        params.put("start",(page-1) * pagesize);
        params.put("pagesize",pagesize);
        //获取总记录数
        int totalCount = courseOrderMapper.findCountByMap(params);
        result.setCount(totalCount);
        //获取查询的数据
        List<CourseOrder> list = courseOrderMapper.findListByMap(params);

        result.setData(list);
        return result;
    }

    @Override
    public void save(CourseOrder order) {
        courseOrderMapper.insert(order);
    }

    @Override
    public CourseOrder findByOrderId(String order_id) {
        return courseOrderMapper.findByOrderId(order_id);
    }

    @Override
    public void deleteByOrderId(String order_id) {
        courseOrderMapper.deleteByOrderId(order_id);
    }

    @Override
    public void update(CourseOrder order) {
        courseOrderMapper.update(order);
    }
}
